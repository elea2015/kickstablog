<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WW84T6');</script>
		<!-- End Google Tag Manager -->
		<meta charset="<?php bloginfo('charset'); ?>">

		<!-- TrustBox script -->
		<script type="text/javascript" src="//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js" async></script>
		<!-- End TrustBox script -->

		<title><?php wp_title(''); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/apple-touch-icon.png" rel="apple-touch-icon-precomposed">

    	<!-- <link rel="stylesheet" type="text/css" href="https://use.typekit.net/whj2hjh.css" /> -->

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	   <!--  <meta name="description" content="<?php //bloginfo('description'); ?>">  -->
		<?php wp_head(); ?>
        <!-- font awesome -->
		<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
		<!-- google fonts -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap" rel="stylesheet">
	</head>
	<body <?php body_class(); ?>>

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WW84T6"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

	  <!-- Load Facebook SDK for JavaScript -->
	  <div id="fb-root"></div>
		<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>

		<div id="mainHeader" class="header sticky-top">
			<header class="container">
			<nav class="navbar navbar-expand-lg navbar-light bg-light px-0">
				<div class="logo-container">
					<a class="navbar-brand" href="https://kicksta.co"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt=""></a>
					<div class="nav-m-btns-container">
						<a href="https://kicksta.co/pricing" class="btn btn-primary">
							<div class="button-text">Start now</div>
						</a>
						<a href="https://dashboard.kicksta.co/login" class="btn btn-blue ml-3">
							<div class="button-text">Log in</div>
						</a>
					</div>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<?php
				wp_nav_menu([
				'menu'            => 'top',
				'theme_location'  => 'header-menu',
				'container'       => 'div',
				'container_id'    => 'bs4navbar',
				'container_class' => 'navbar-nav m-auto',
				'menu_id'         => false,
				'menu_class'      => 'navbar-nav m-auto',
				'depth'           => 2,
				'fallback_cb'     => 'bs4navwalker::fallback',
				'walker'          => new bs4navwalker()
				]);
				?>
				<?php
		        	$emailField = ($_GET["emailField"]);
		    	 ?>
				<a class="btn" href="https://dashboard.kicksta.co/login">Login</a>
				<a class="btn btn-primary" href="https://kicksta.co/pricing">Start my growth</a>
				</div>
			</nav>
			</header>
		</div>
		
		<?php
			$recent_posts = wp_get_recent_posts(array(
				'numberposts' => 1,
				'post_status' => 'publish' 
			));
			$recent_id = $recent_posts[0]['ID'];
		?>

		<script>
			//select dropdowwn
			dropdown = document.querySelector('.dropdown-menu');
			//append html and php
			dropdown.innerHTML += '<div class="dropdown-mega"><span>Most Popular Blog</span><p>Best Instagram Growth Service In 2023: We Compare The 25 Top Tools</p><a href="https://blog.kicksta.co/best-instagram-growth-service/">Read More</a> <span>Latest Blog</span><p>' + '<?php echo get_the_title($recent_id) ?>' + '</p><a href="' + '<?php echo esc_url( get_permalink($recent_id) ) ?>' + '">Read More</a></div>'
		</script>