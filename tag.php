<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="blogList">

			<h1 class="text-center py-5"><?php _e( 'Tag: ', 'html5blank' ); echo single_tag_title('', false); ?></h1>

			<?php get_template_part('customloopv2'); ?>

		</section>
		<!-- /section -->
		<section class="blogPagination py-5">
			<?php get_template_part('pagination'); ?>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
