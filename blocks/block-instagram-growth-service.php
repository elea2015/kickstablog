<div class="cta-service__container">
    <div class="cta-service__image">
        <img src="<?php echo get_template_directory_uri()?>/img/logo-line.png" alt="Kicksta logo">
    </div>
    <div class="cta-service__headline">
        <h4>You Now Know The Best Time To Post, But Do You Know The Best Growth Method?</h4>
        <a href="https://blog.kicksta.co/best-instagram-growth-service/?utm_source=blog+page&utm_medium=CTA+element&utm_campaign=best-time-to-best-ig-referral">Learn More Here</a>
    </div>
</div>