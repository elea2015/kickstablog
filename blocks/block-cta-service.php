<div class="cta-service__container">
    <div class="cta-service__image">
        <img src="<?php echo get_template_directory_uri()?>/img/logo-line.png" alt="Kicksta logo">
    </div>
    <div class="cta-service__headline">
        <h4><?php block_field( 'heading' ); ?></h4>
        <a href="https://kicksta.co/pricing">Try Kicksta Today!</a>
    </div>
</div>