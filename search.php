<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="blogList">

			<h2 class="text-center py-5"><?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h2>

			<?php get_template_part('customloop'); ?>

		</section>
		<!-- /section -->
		<section class="blogPagination py-5">
			<?php get_template_part('pagination'); ?>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
