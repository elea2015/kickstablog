<footer style="background:#ffffff" class="">
  <div class="container">
    <div class="footerFlex">
      <div class="mb-3 px-0 footer1">
        <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt=""></a>
        <p class="mt-2">Made with ❤️ in San Diego, CA</p>      
      </div>
      <div class="footerMenu">
        <div class="footer2">
          <ul class="list-unstyled">
            <li><a href="https://kicksta.co/how-it-works">FAQs</a></li>
            <li><a class="disable-link" href="/">Compare:</a></li>
            <li><a href="https://kicksta.co/instagram-marketing/upleap-vs-kicksta">- Upleap</a></li>
            <li><a href="https://kicksta.co/instagram-marketing/buzzoid-review-kicksta-comparison">- Buzzoid</a></li>
            <li><a href="https://kicksta.co/instagram-marketing/mr-insta-vs-kicksta">- Mr. Insta</a></li>
            <li><a href="https://kicksta.co/instagram-marketing/nitreo-vs-kicksta">- Nitreo</a></li>
          </ul>
        </div>
        <div class="footer3">
          <ul class="list-unstyled">
            <li><a href="/">Blog</a></li>
            <li><a href="https://kicksta.co/pricing">Pricing</a></li>
            <li><a href="https://kicksta.co/affiliate">Affiliates</a></li>
            <li><a href="https://kicksta.co/writers">Writers</a></li>
            <li><a href="https://kicksta.co/contact">Contact</a></li>
          </ul>
        </div>
        <div class="footer4">
          <ul class="list-unstyled">
            <li><a href="https://kicksta.co/billing">Billing policy</a></li>
            <li><a href="https://kicksta.co/privacy-policy">Privacy policy</a></li>
            <li><a href="https://kicksta.co/terms-of-service">Terms of service</a></li>
            <li><a href="https://kicksta.co/kicksta-reviews">Kicksta reviews</a></li>
            <li><a href="https://kicksta.co/instagram-infographic-library">Instagram Infographics</a></li>
          </ul>
        </div>
      </div>
      <div class="footer5">
        <p>Get advanced Instagram growth strategies</p>
        <div class="ctaSubscribe ctaSubscribeP">
            <div class="ctaRow">
                <?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]')?>
            </div>
        </div>
     
      <!-- social -->
      <div class="socialFooter mt-2">
        <p>Follow us on social media:</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="https://www.instagram.com/kicksta.co/" target="_blank">
              <img src="<?php echo get_template_directory_uri(); ?>/img/Instagramicon.svg" alt="">
            </a>
          </li>
        </ul>
      </div>
      <!-- social -->
      </div>
    </div>
  </div>
</footer>

<section class="legal text-center py-3">
  <div class="made-mobile">
    <a href="https://www.instagram.com/kicksta.co/" target="_blank">
      <img src="<?php echo get_template_directory_uri(); ?>/img/Instagramicon.svg" alt="">
    </a>
    <p class="mt-2">Made with ❤️ in San Diego, CA</p>
  </div>
  <ul class="list-inline">
    <li class="list-inline-item">&copy; <?php echo date('Y');?></li>
    <li class="list-inline-item">
      <a href="https://kicksta.co/terms-of-service">Terms of service</a>
    </li>
  </ul>
</section>

		<?php wp_footer(); ?>
    <!-- <script src="<?php //echo get_template_directory_uri(); ?>/js/infinite.js"></script> -->
   <!--  <script src="<?php //echo get_template_directory_uri(); ?>/js/myscript.js"></script> -->

<script>
  window.intercomSettings = {
    app_id: "hg32et9j"
  };
</script>
<!-- <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/hg32et9j';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
<script src="//rum-static.pingdom.net/pa-5b463bfcef13ce0016000164.js" async></script> -->
	</body>
</html>
