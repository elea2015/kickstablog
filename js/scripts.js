(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		// DOM ready, take it away
		$(document).ready(function () {
            //$(".socialSharePostList").hide();
            // $('#menu-item-6215 a').click(function(event){
            //     event.preventDefault();
            //     $('#searchFormContainer').slideToggle(100);
            // });
            $('.blogAction a.socialSharePost').click(function (event) {
                event.preventDefault();
                var shareContainer = $(this).parent();
                var shareList = $(shareContainer).find('.socialSharePostList');
                $(this).hide();
                $(shareList).show();
            });
        
            // Email form validation
            $('.getStartedForm .btn').click(function (event) {
                var form = $(this).parents('form:first') // Get Button Form Parent
                var emailField = $(form).find('.emailField') // Get email field
                if ($(emailField).val() == '') { // Field validation
                    $(form).addClass('animated shake') // Adds shake animation
                    $(emailField).addClass('redError') // Adds Red border
                    // When animation stops removed animations classes
                    $(form).one('webkitAnimationEnd` mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                        $(this).removeClass('animated shake')
                        $(emailField).removeClass('redError')
                    })
                    event.preventDefault()
                }3
            }); // ends email field empty
        
            
            $('#btnValidate').click(function(event) {
                var sEmail = $('#txtEmail').val();
                if ($.trim(sEmail).length == 0) {
                    $('#txtEmail').addClass('redError');
                    event.preventDefault();
                }
                if (validateEmail(sEmail)) {
                    event.preventDefault();
                }
                else {
                    $('#txtEmail').addClass('redError');
                }
            });
            
            //custom cta for white lable post
            $(".postid-6065 .ctaSubscribe .ctaRow h2").text('Want white label Instagram automation for your digital agency ?');
            $(".postid-6065 .ctaSubscribe .ctaRow p").text('See how Kicksta can save you time and increase your reccurring monthly revenue.');
            //$(".postid-6065 .ctaSubscribe .ctaRow .formStarted").replaceWith('<a target="_blank" href="https://kicksta.co/contact" class="btn btn-secondary mx-auto">Request a demo<a/>');
            $(".postid-6065 .ctaSubscribe .ctaRow .formStarted .btn").text('Request a demo');
            $(".postid-6065 .ctaSubscribe .ctaRow .formStarted").attr('action', 'https://kicksta.co/contact');

            //$('.dropdown-menu').append('<div class="dropdown-mega"><span>Most Popular Blog</span><p>Best Instagram Growth Service In 2022: We Compare The 21 Top Tools</p><a href="https://blog.kicksta.co/best-instagram-growth-service/">Read More</a><span>Latest Blog</span><p>10 Crucial Social Media KPIs You Should Be Tracking</p><a href="https://blog.kicksta.co/social-media-kpis-you-should-be-tracking/">Read More</a></div>');
            $('.disable-link').click(function (e){
                e.preventDefault();
            })

            $('.navbar-toggler').click(function (e){
                $('.nav-m-btns-container').toggleClass('show-nav')
            })
        
        });
        
        
        function validateEmail(sEmail) {
            var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (filter.test(sEmail)) {
                return true;
            }
            else {
                return false;
            }
        }
		
	});
	
})(jQuery, this);
